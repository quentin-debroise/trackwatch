
TARGET = test
TEMPLATE = app

CONFIG += warn_on
CONFIG += c++11

QT += core gui widgets network sql

QMAKE_CXXFLAGS_DEBUG -= -O1
QMAKE_CXXFLAGS_DEBUG -= -O2
QMAKE_CXXFLAGS_DEBUG -= -O3
QMAKE_CXXFLAGS_DEBUG *= -O0

INCLUDEPATH += ./include 

SOURCES += \
    src/main.cpp    \
    src/tvmazewrapper.cpp \
    src/requestsender.cpp \
    src/models/searchresults.cpp \
    src/views/thumbnailview.cpp \
    src/thumbnailcache.cpp \
    src/models/show.cpp \
    src/models/episode.cpp \
    src/presenters/showinformation.cpp \
    src/presenters/search.cpp \
    src/presenters/mainwindow.cpp \
    src/presenters/episodetracker.cpp \
    src/query/whereconditiondao.cpp \
    src/presenters/subscription.cpp \
    src/views/expandablepanel.cpp \
    src/views/episodeview.cpp \
    src/views/trackbutton.cpp \
    src/presenters/header.cpp \
    src/query/querybuilder.cpp \
    src/models/showepisode.cpp \
    src/query/selectstatement.cpp \
    src/query/insertstatement.cpp \
    src/models/subscriptionmodel.cpp \
    src/query/deletestatement.cpp \
    src/query/updatestatement.cpp

HEADERS += \
    include/tvmazewrapper.h \
    include/apiwrapper.h \
    include/requestsender.h \
    include/models/searchresults.h \
    include/views/thumbnailview.h \
    include/thumbnailcache.h \
    include/query/whereconditiondao.h \
    include/models/show.h \
    include/models/episode.h \
    include/presenters/episodetracker.h \
    include/presenters/mainwindow.h \
    include/presenters/search.h \
    include/presenters/showinformation.h \
    include/presenters/subscription.h \
    include/views/expandablepanel.h \
    include/views/episodeview.h \
    include/views/trackbutton.h \
    include/presenters/header.h \
    include/query/querybuilder.h \
    include/models/showepisode.h \
    include/query/selectstatement.h \
    include/query/insertstatement.h \
    include/models/subscriptionmodel.h \
    include/query/deletestatement.h \
    include/query/updatestatement.h


MOC_DIR = moc
OBJECTS_DIR = obj
RCC_DIR = rcc
UI_DIR = ui

FORMS += \
    forms/mainwindow.ui \
    forms/searchview.ui \
    forms/showinformationview.ui \
    forms/thumbnailview.ui \
    forms/episodetrackerview.ui \
    forms/episodeview.ui \
    forms/form.ui \
    forms/headerview.ui \
    forms/subscriptionview.ui

OTHER_FILES +=

RESOURCES += \
    resources.qrc
