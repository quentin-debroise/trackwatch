#include <QDebug>
#include <QUrl>
#include <QImage>
#include <QJsonDocument>
#include <QJsonArray>
#include <QJsonObject>
#include <memory>

#include <requestsender.h>
#include <models/show.h>
#include <tvmazewrapper.h>

QString TVMazeWrapper::API_URL = "http://api.tvmaze.com";

TVMazeWrapper::TVMazeWrapper(){}
TVMazeWrapper::~TVMazeWrapper(){}

void TVMazeWrapper::search_show(const QString& show){
    const QUrl ENDPOINT(API_URL + "/search/shows");
    std::map<QString, QString> params;
    params["q"] = show;
    RequestSender *rq = new RequestSender();
    rq->request(ENDPOINT, params);
    rq->send();
    connect(rq, SIGNAL(request_success(QByteArray, std::map<QString, QVariant>)),
            this, SLOT(parse_search(QByteArray, std::map<QString, QVariant>)));
    /* TODO: delete rq somewhere ?! */
}
void TVMazeWrapper::parse_search(QByteArray data, std::map<QString, QVariant> properties){
    std::vector<std::shared_ptr<Show>> shows;
    QJsonDocument doc = QJsonDocument::fromJson(data);
    QJsonArray series = doc.array();
    auto it = series.begin();
    while(it != series.end()){
        QJsonObject show = (*it).toObject()["show"].toObject();
        shows.push_back(std::shared_ptr<Show>(new Show(
                show["id"].toInt(),
                show["name"].toString(),
                show["status"].toString(),
                show["summary"].toString(),
                show["image"].toObject()["medium"].toString())));
        it++;
    }

    emit receive_shows(shows);
}

void TVMazeWrapper::thumbnail(long showid, const QString& imgurl){
    RequestSender *rq = new RequestSender();
    rq->request(imgurl);
    rq->send();
    rq->add_property("showid", QVariant::fromValue(showid));
    rq->add_property("imgurl", QVariant::fromValue(imgurl));
    connect(rq, SIGNAL(request_success(QByteArray, std::map<QString, QVariant>)),
            this, SLOT(parse_thumbnail(QByteArray, std::map<QString, QVariant>)));
}
void TVMazeWrapper::parse_thumbnail(QByteArray data, std::map<QString, QVariant> properties){
    QImage image;
    image.loadFromData(data);

    if(image.isNull()){
        qDebug() << "tvmazewrapper.cpp [WARNING]: Can't load image";
    }

    emit receive_thumbnail(image, properties);
}

void TVMazeWrapper::get_show(long showid){
    const QUrl ENDPOINT(API_URL + "/shows/" + QString::number(showid));
    // std::map<QString, QString> params;
    // params["embed"] = "episodes";
    RequestSender *rq = new RequestSender();
    // rq->request(ENDPOINT, params);
    rq->request(ENDPOINT);
    rq->send();
    connect(rq, SIGNAL(request_success(QByteArray, std::map<QString,QVariant>)),
            this, SLOT(parse_single_show(QByteArray, std::map<QString,QVariant>)));
}
void TVMazeWrapper::parse_single_show(QByteArray data, std::map<QString, QVariant> properties){
    QJsonDocument doc = QJsonDocument::fromJson(data);
    QJsonObject showjson = doc.object();

    Show show = Show(
            showjson["id"].toInt(),
            showjson["name"].toString(),
            showjson["status"].toString(),
            showjson["summary"].toString(),
            showjson["image"].toObject()["medium"].toString());

    emit receive_single_show(show, properties);
}

void TVMazeWrapper::get_episodes(long showid){
    const QUrl ENDPOINT(API_URL + "/shows/" + QString::number(showid) + "/episodes");
    RequestSender *rq = new RequestSender();
    rq->request(ENDPOINT);
    rq->add_property("showid", QVariant::fromValue(showid));
    rq->send();
    connect(rq, SIGNAL(request_success(QByteArray, std::map<QString,QVariant>)),
            this, SLOT(parse_episodes(QByteArray, std::map<QString,QVariant>)));
}
void TVMazeWrapper::parse_episodes(QByteArray data, std::map<QString, QVariant> properties){
    QJsonDocument doc = QJsonDocument::fromJson(data);
    QJsonArray json = doc.array();

    QList<Episode> episodes;
    auto it = json.begin();
    while(it != json.end()){
        QJsonObject ep = (*it).toObject();
        episodes.append(Episode(
            ep["id"].toInt(),
            ep["name"].toString(),
            ep["season"].toInt(),
            ep["number"].toInt(),
            ep["runtime"].toInt()
        ));
        it++;
    }
    emit receive_episodes(episodes, properties);
}
