#include <QDebug>
#include <views/episodeview.h>
#include <ui_episodeview.h>

EpisodeView::EpisodeView(QString title, int numberInSeason, QWidget *parent):
    QWidget(parent),
    ui(new Ui::EpisodeView){
    ui->setupUi(this);
    ui->_title->setText(title);
    ui->_episodeNumber->setText(QString::number(numberInSeason));

    connect(ui->_watched, SIGNAL(clicked(bool)), this, SLOT(episode_watch_update(bool)));
}

void EpisodeView::episode_watch_update(bool checked){
    if(checked){
        qDebug() << "Set episode has been watched";
    }
    else{
        qDebug() << "Set episode has not been watched";
    }
}
