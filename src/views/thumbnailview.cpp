#include <QDebug>
#include <QFontMetrics>
#include <QMouseEvent>

#include <views/thumbnailview.h>
#include <ui_thumbnailview.h>


ThumbnailView::ThumbnailView(QWidget *parent):
    QWidget(parent),
    ui(new Ui::ThumbnailView),
    _showid(-1){
    ui->setupUi(this);
}

void ThumbnailView::set_title(const QString& title){
    QFontMetrics fm(ui->_label->font());
    if(fm.width(title) > 105){
        int avgCharWidth = fm.averageCharWidth();
        int displayedChar = 105 / avgCharWidth - 3;
        ui->_label->setText(title.mid(0, displayedChar) + "...");
    }
    else{
        ui->_label->setText(title);
    }
    setToolTip(title);
}

void ThumbnailView::set_image(const QImage& image){
    ui->_imgcontainer->setPixmap(QPixmap::fromImage(image));
}

void ThumbnailView::mousePressEvent(QMouseEvent *event){
    if(event->buttons() == Qt::LeftButton && ui->_imgcontainer->underMouse()){
        emit thumbnail_clicked(_showid);
    }
    event->accept();
}
