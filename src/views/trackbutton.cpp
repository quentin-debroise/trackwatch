#include <QDebug>
#include <QWidget>
#include <QLabel>
#include <QPixmap>
#include <QHBoxLayout>
#include <QMouseEvent>
#include <QEvent>
#include <QStyle>
#include <QStyleOption>
#include <QPainter>
#include <QFontMetrics>
#include <views/trackbutton.h>

namespace{
    QString CHECKED_TEXT = QObject::tr("Tracking");
    QString UNCHECKED_TEXT = QObject::tr("Track");
}

TrackButton::TrackButton(QWidget *parent):
    QWidget(parent),
    _layout(new QHBoxLayout),
    _before(new QLabel),
    _text(new QLabel),
    _hovering(false){

    setup_ui();
    set_checked(false);
}

TrackButton::~TrackButton(){
    delete _layout;
    delete _before;
    delete _text;
}

void TrackButton::set_checked(bool checked){
    handle_check(checked);
}

void TrackButton::mousePressEvent(QMouseEvent *event){
    handle_check(!_checked);
    event->accept();
    emit clicked();
}

void TrackButton::handle_check(bool checked){
    _checked = checked;
    if(_checked){
        _text->setText(CHECKED_TEXT);
        _text->setProperty("checked", true);
        _before->setProperty("checked", true);
        _before->setPixmap(QPixmap(":/icons/eye.svg"));
    }
    else{
        _text->setText(UNCHECKED_TEXT);
        _text->setProperty("checked", false);
        _before->setProperty("checked", false);
        _before->setPixmap(QPixmap(":/icons/plus.svg"));
    }
    _text->style()->unpolish(_text);
    _text->style()->polish(_text);
    _before->style()->unpolish(_before);
    _before->style()->polish(_before);
}

void TrackButton::setup_ui(){
    QFontMetrics fm(_text->font());
    _before->setObjectName("trackbutton-left");
    _before->setMinimumHeight(40);
    _before->setMaximumHeight(40);
    _before->setFixedWidth(50);
    _before->setPixmap(QPixmap(":/icons/plus.svg"));
    _before->setScaledContents(true);

    _text->setObjectName("trackbutton-right");
    _text->setMinimumHeight(40);
    _text->setMaximumHeight(40);
    _text->setMinimumWidth(fm.width(CHECKED_TEXT) + 10);
    _text->setText(UNCHECKED_TEXT);
    _text->setAlignment(Qt::AlignCenter);

    _layout->setMargin(0);
    _layout->setSpacing(0);

    _layout->addWidget(_before);
    _layout->addWidget(_text);

    _before->installEventFilter(this);
    _text->installEventFilter(this);

    setLayout(_layout);
    show();
}

bool TrackButton::eventFilter(QObject *target, QEvent *event){
    /* TODO: distinguish entering/leaving and the two Label to set _hovering correctly */
    /* if target is _before then test Enter/Leave
     * else if target is _text then test Enter/Leave
     */
    if(target == _before || target == _text){
        if(event->type() == QEvent::Enter){
            if(!_hovering){
                _hovering = true;
            }
            return true;
        }
        else if(event->type() == QEvent::Leave){
            _hovering = false;
            return true;
        }
    }
    return QWidget::eventFilter(target, event);
}

void TrackButton::paintEvent(QPaintEvent *event){
    QStyleOption opt;
    opt.init(this);
    QPainter p(this);
    style()->drawPrimitive(QStyle::PE_Widget, &opt, &p, this);
}
