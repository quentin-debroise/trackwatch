#include <QDebug>
#include <QLabel>
#include <QHeaderView>
#include <QCheckBox>
#include <QPropertyAnimation>
#include <QVBoxLayout>
#include <views/expandablepanel.h>

#include <views/trackbutton.h>

ExpandablePanel::ExpandablePanel(QWidget *parent):
    QWidget(parent){
    setup_ui();
    animating = false;
    connect(_category, SIGNAL(clicked()), this, SLOT(toggle()));
}
ExpandablePanel::~ExpandablePanel(){
    delete _category;
    delete _hidden;
    delete _layout;
}

void ExpandablePanel::setup_ui(){
    _category = new QPushButton(tr("Toggle"), this);
    _hidden = new QTableWidget(this);
    _layout = new QVBoxLayout(this);

    _hidden->horizontalHeader()->hide();
    _hidden->verticalHeader()->hide();
    _hidden->horizontalHeader()->setSectionResizeMode(QHeaderView::ResizeToContents);
    _hidden->horizontalHeader()->setStretchLastSection(true);
    _hidden->setVerticalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
    _hidden->setHorizontalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
    _hidden->setSelectionMode(QAbstractItemView::NoSelection);
    _hidden->setShowGrid(false);
    _hidden->setColumnCount(3);
    _hidden->setVisible(false);

    _layout->setMargin(0);
    _layout->addWidget(_category);
    _layout->addWidget(_hidden);
    _layout->addSpacerItem(new QSpacerItem(0, 0, QSizePolicy::Expanding, QSizePolicy::Expanding));

    setLayout(_layout);
    show();
}

void ExpandablePanel::toggle(){
    if(_hidden->isVisible()){
        _hidden->setVisible(false);
        /*
        QPropertyAnimation *animation = new QPropertyAnimation(_hidden, "maximumHeight", _hidden);
        animation->setDuration(1000);
        animation->setEasingCurve(QEasingCurve::InOutQuad);
        animation->setStartValue(_hidden->model()->rowCount() * _hidden->rowHeight(0));
        animation->setEndValue(0);
        connect(animation, SIGNAL(finished()), this, SLOT(finished_collapsing()));
        animation->start(QPropertyAnimation::DeleteWhenStopped);
        */
    }
    else{
        _hidden->setVisible(true);
        /*
        QPropertyAnimation *animation = new QPropertyAnimation(_hidden, "minimumHeight", _hidden);
        animation->setDuration(1000);
        animation->setEasingCurve(QEasingCurve::InOutQuad);
        animation->setStartValue(0);
        animation->setEndValue(_hidden->model()->rowCount() * _hidden->rowHeight(0));
        animation->start(QPropertyAnimation::DeleteWhenStopped);
        */
    }
}

void ExpandablePanel::finished_collapsing(){
    _hidden->setVisible(false);
    repaint();
}
void ExpandablePanel::finished_expanding(){
}

void ExpandablePanel::set_category(QString name){
    _category->setText(name);
}

void ExpandablePanel::add(int episodeId, int numberInSeason, QString title, bool checked){
        const int row = _hidden->model()->rowCount();
        _hidden->insertRow(row);
        _hidden->setCellWidget(row, 0, new QLabel(QString::number(numberInSeason), _hidden));
        _hidden->setCellWidget(row, 1, new QLabel(title, _hidden));
        QCheckBox *cb = new QCheckBox(_hidden);
        cb->setChecked(checked);
        cb->setStyleSheet(
                "QCheckBox::indicator{"
                    "width: 50px;"
                    "height: 50px;"
                "}"
                "QCheckBox{"
                    "width: 50px;"
                    "height: 50px;"
                "}"
        );
        cb->style()->unpolish(cb);
        cb->style()->polish(cb);
        cb->setProperty("episodeId", episodeId);
        connect(cb, SIGNAL(clicked(bool)), this, SLOT(clicked(bool)));
        _hidden->setCellWidget(row, 2, cb);
        _hidden->setMinimumHeight(_hidden->rowHeight(0) * _hidden->model()->rowCount());
}

void ExpandablePanel::clicked(bool active){
    QCheckBox *cb = qobject_cast<QCheckBox*>(sender());
    const int episodeId = cb->property("episodeId").value<int>();
    emit watched(episodeId, active);
}
