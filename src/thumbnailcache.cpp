#include <QDebug>
#include <QString>
#include <QImage>
#include <QPixmap>
#include <QDir>
#include <QFile>
#include <QDataStream>

#include <include/thumbnailcache.h>

namespace{
    const QString THUMBNAIL_CACHE_DIR = ".thumbnail/";
}

void ThumbnailCache::init(){
    if(!QDir(THUMBNAIL_CACHE_DIR).exists()){
        QDir().mkdir(THUMBNAIL_CACHE_DIR);
    }
}

void ThumbnailCache::cache(const QString& filename, const QImage& imgdata){
    if(filename.isEmpty()){
        qDebug() << "thumbnailcache.cpp [WARNING]: Can't cache image with empty name";
        return;
    }

    imgdata.save(THUMBNAIL_CACHE_DIR + filename, "JPG");

    /*
    QFile file(THUMBNAIL_CACHE_DIR + filename);
    file.open(QIODevice::WriteOnly);
    QDataStream out(&file);
    out << imgdata;
    file.close();
    */
}

bool ThumbnailCache::has(const QString& filename){
    QString path = THUMBNAIL_CACHE_DIR + filename;
    return QFile(path).exists();
}

QImage ThumbnailCache::load(const QString& filename){
    QImage thumbnail;
    thumbnail.load(THUMBNAIL_CACHE_DIR + filename, "JPG");
    return thumbnail;

    /*
    QFile file(THUMBNAIL_CACHE_DIR + filename);
    if(file.open(QIODevice::ReadOnly)){
        QDataStream in(&file);
        in >> thumbnail;
        file.close();
        if(thumbnail.isNull()){
            qDebug() << ">>>> ERROR\n";
        }
    }
    return thumbnail;
    */
}
