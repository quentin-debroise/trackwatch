#include <query/selectstatement.h>

SelectStatement SelectStatement::select(QString tablename, const QStringList& attributes){
    _tablename = tablename;
    _attributes = attributes;
    if(_attributes.isEmpty()){
        _attributes.append(QStringLiteral("*"));
    }
    return *this;
}

SelectStatement SelectStatement::where(QString attribute, QVariant value){
    _where.clear();
    _where.append(WhereConditionDAO(
                      attribute, value,
                      WhereConditionDAO::OP_EQ, WhereConditionDAO::LOGIC_NONE));
    return *this;
}
SelectStatement SelectStatement::w_and(QString attribute, QVariant value){
    _where.append(WhereConditionDAO(
                      attribute, value,
                      WhereConditionDAO::OP_EQ, WhereConditionDAO::LOGIC_AND));
    return *this;
}
SelectStatement SelectStatement::w_or(QString attribute, QVariant value){
    _where.append(WhereConditionDAO(
                      attribute, value,
                      WhereConditionDAO::OP_EQ, WhereConditionDAO::LOGIC_AND));
    return *this;
}

QString SelectStatement::toString() const{
    QString where = _where.isEmpty() ? "" : "WHERE";
    for (int i = 0; i < _where.count(); i++){
        where += _where.at(i).toString();
    }
    return QString("SELECT %1 FROM %2 %3").arg(
                _attributes.join(","),
                _tablename,
                where);
}

QSqlQuery SelectStatement::get() const{
    QSqlQuery query;
    query.prepare(toString());
    for (int i = 0; i < _where.count(); i++){
        query.bindValue(_where.at(i).placeholder(), _where.at(i).value());
    }
    return query;
}

void SelectStatement::clear(){
    _tablename = "";
    _attributes.clear();
    _where.clear();
}
