#include <QDebug>
#include <query/updatestatement.h>

UpdateStatement& UpdateStatement::update(QString tablename){
    clear();
    _tablename = tablename;
    return *this;
}

UpdateStatement& UpdateStatement::set(QString column, QVariant newvalue){
    _set[column] = newvalue;
    return *this;
}

UpdateStatement& UpdateStatement::where(QString attribute, QVariant value){
    _where.clear();
    _where.append(WhereConditionDAO(
                      attribute, value,
                      WhereConditionDAO::OP_EQ, WhereConditionDAO::LOGIC_NONE));
    return *this;
}

QSqlQuery UpdateStatement::get() const{
    QSqlQuery query;
    query.prepare(toString());
    QMap<QString, QVariant>::const_iterator it;
    for (it = _set.constBegin(); it != _set.constEnd(); it++){
        query.bindValue(QString(":%1_u").arg(it.key()), it.value());
    }
    for (int i = 0; i < _where.count(); i++){
        query.bindValue(_where.at(i).placeholder(), _where.at(i).value());
    }
    return query;
}

void UpdateStatement::clear(){
    _tablename.clear();
    _set.clear();
    _where.clear();
}

QString UpdateStatement::toString() const{
    QString where = _where.isEmpty() ? "" : "WHERE";
    for (int i = 0; i < _where.count(); i++){
        where += _where.at(i).toString();
    }

    QMap<QString, QVariant>::const_iterator it;
    QStringList set;
    for (it = _set.constBegin(); it != _set.constEnd(); it++){
        set.append(QString("%1 = :%2_u").arg(it.key(), it.key()));
    }

    return QString("UPDATE %1 SET %2 %3").arg(_tablename, set.join(","), where);
}
