#include <QDebug>
#include <QSqlRecord>
#include <query/insertstatement.h>

InsertStatement& InsertStatement::insert(QString tablename){
    clear();
    _tablename = tablename;
    return *this;
}

InsertStatement& InsertStatement::columns(QStringList columns){
    _columns = columns;
    if(_columns.isEmpty()){
        QSqlRecord record = QSqlDatabase::database().record(_tablename);
        for (int i = 0; i < record.count(); i++){
            _columns.append(record.fieldName(i));
        }
    }
    return *this;
}

InsertStatement& InsertStatement::values(QList<QVariant> values){
    _values = values;
    return *this;
}

QSqlQuery InsertStatement::get() const{
    Q_ASSERT(_values.count() == _columns.count());
    QSqlQuery query;
    query.prepare(toString());
    for (int i = 0; i < _values.count(); i++){
        query.bindValue(":" + _columns.at(i), _values.at(i));
    }
    return query;
}

void InsertStatement::clear(){
    _tablename = "";
    _columns.clear();
    _values.clear();
}

QString InsertStatement::toString() const{
    QStringList placeholders;
    for (int i = 0; i < _columns.count(); i++){
        placeholders.append(":" + _columns.at(i));
    }
    return QString("INSERT INTO %1(%2) VALUES(%3)").arg(_tablename, _columns.join(","), placeholders.join(","));
}
