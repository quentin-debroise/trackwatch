#include <query/whereconditiondao.h>

namespace{
    const QStringList STR_OP{"=", "!=", "<", ">", "<=", ">="};
    const QStringList STR_LOGIC{"", "AND", "OR", "IN", "LIKE"};
}

WhereConditionDAO::WhereConditionDAO(
        QString columnname,
        QVariant bindvalue,
        WhereOp op,
        WhereLogic logic,
        bool subquery):
    _columnname(columnname),
    _bindvalue(bindvalue),
    _op(op),
    _logic(logic),
    _subquery(subquery){
}

QString WhereConditionDAO::toString() const{
    QString strLogic = STR_LOGIC.at(static_cast<int>(_logic));
    QString strOp = STR_OP.at(static_cast<int>(_op));
    /*
    QString strLogic = "";
    QString strOp = "=";
    if (_logic == WhereLogic::LOGIC_AND){
        strLogic = " AND";
    }
    else if (_logic == WhereLogic::LOGIC_OR){
        strLogic = " OR";
    }

    switch (_op){
        case WhereOp::OP_GE:
            strOp = ">=";
            break;
        case WhereOp::OP_LE:
            strOp = "<=";
            break;
        case WhereOp::OP_GT:
            strOp = ">";
            break;
        case WhereOp::OP_LT:
            strOp = "<";
            break;
        case WhereOp::OP_NE:
            strOp = "!=";
            break;
        default: break;
    }
    */

    if(_subquery){
        return QString(" %1 %2 %3 (%4)").arg(
                    strLogic,
                    _columnname,
                    strOp,
                    _bindvalue.value<QString>());
    }
    return QString(" %1 %2 %3 :%4").arg(
                strLogic,
                _columnname,
                strOp,
                _columnname);
}
