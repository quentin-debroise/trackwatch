#include <query/deletestatement.h>


DeleteStatement& DeleteStatement::remove(QString tablename){
    clear();
    _tablename = tablename;
    return *this;
}

DeleteStatement& DeleteStatement::where(QString attribute, QVariant value){
    _where.clear();
    _where.append(WhereConditionDAO(
                      attribute, value,
                      WhereConditionDAO::OP_EQ, WhereConditionDAO::LOGIC_NONE));
    return *this;
}

QSqlQuery DeleteStatement::get() const{
    QSqlQuery query;
    query.prepare(toString());
    for (int i = 0; i < _where.count(); i++){
        query.bindValue(_where.at(i).placeholder(), _where.at(i).value());
    }
    return query;
}

QString DeleteStatement::toString() const{
    QString where = _where.isEmpty() ? "" : "WHERE";
    for (int i = 0; i < _where.count(); i++){
        where += _where.at(i).toString();
    }
    return QString("DELETE FROM %1 %2").arg(_tablename, where);
}

void DeleteStatement::clear(){
    _tablename.clear();
    _where.clear();
}
