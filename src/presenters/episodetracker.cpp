#include <QDebug>
#include <QPixmap>
#include <QObjectList>
#include <thumbnailcache.h>
#include <models/showepisode.h>
#include <views/episodeview.h>
#include <presenters/episodetracker.h>
#include <ui_episodetrackerview.h>

#include <views/expandablepanel.h>

EpisodeTracker::EpisodeTracker(QWidget *parent):
    QWidget(parent),
    ui(new Ui::EpisodeTrackerView){
    ui->setupUi(this);

    connect(ui->_back, SIGNAL(clicked()), this, SLOT(back_clicked()));
}
EpisodeTracker::~EpisodeTracker(){
    delete ui;
}

void EpisodeTracker::back_clicked(){
    /* TODO: Clear scroll area here ? */
    emit back();
}

void EpisodeTracker::track(long showid){
    Show s = Show::load(showid);
    ui->_title->setText(s.title());
    // ui->_shortDescription->setText(s.summary());
    if(ThumbnailCache::has(s.imgpath())){
        QImage thumbnail = ThumbnailCache::load(s.imgpath());
        ui->_thumbnail->setPixmap(QPixmap::fromImage(thumbnail));
    }
    else{
        /* TODO: redownload image */
    }

    QList<Episode> episodes = s.episodes();
    // std::vector<Episode>::const_iterator it;
    QList<Episode>::const_iterator it = episodes.cbegin();
    int currentSeason = -1;
    ExpandablePanel *exp = nullptr;

    clear();
    for(it = episodes.begin(); it != episodes.end(); it++){
        if(it->season() != currentSeason){
            if(currentSeason != -1){
                ui->_scrollarea->layout()->addWidget(exp);
            }
            currentSeason = it->season();
            exp = new ExpandablePanel(ui->_scrollarea);
            connect(exp, SIGNAL(watched(qlonglong, bool)), this, SLOT(watched(qlonglong, bool)));
            exp->set_category(tr("Season ") + QString::number(currentSeason));
        }
        const bool watched = ShowEpisode::load(it->id()).watched();
        exp->add(it->id(), it->number_in_season(), it->title(), watched);
    }
    if(exp){
        ui->_scrollarea->layout()->addWidget(exp);
    }
    // ui->_scrollarea->layout()->addWidget(new QSpacerItem(0, 0, QSizePolicy::Expanding, QSizePolicy::Expanding));
    ui->_scrollarea->layout()->addItem(new QSpacerItem(0, 0, QSizePolicy::Expanding, QSizePolicy::Expanding));

    emit show_tracker();
}

void EpisodeTracker::watched(qlonglong episodeId, bool watched){
    ShowEpisode::load(episodeId).update({{"watched", watched}});
}


void EpisodeTracker::clear(){
    QObjectList widgets = ui->_scrollarea->children();
    while(!widgets.isEmpty()){
        delete qobject_cast<ExpandablePanel*>(widgets.takeAt(0));
    }
}
