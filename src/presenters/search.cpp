#include <QDebug>
#include <QImage>
#include <QLabel>
#include <QLayoutItem>
#include <QMouseEvent>

#include <models/searchresults.h>
#include <tvmazewrapper.h>
#include <requestsender.h>
#include <thumbnailcache.h>
#include <views/thumbnailview.h>
#include <presenters/search.h>
#include <ui_searchview.h>


Search::Search(QWidget *parent):
    QWidget(parent),
    ui(new Ui::SearchView),
    _api(std::unique_ptr<APIWrapper>(new TVMazeWrapper)){
    ui->setupUi(this);

    connect(ui->_searchButton, SIGNAL(clicked()), this, SLOT(search()));
    connect(ui->_searchbar, SIGNAL(returnPressed()), this, SLOT(search()));

    connect(_api.get(), SIGNAL(receive_shows(const std::vector<std::shared_ptr<Show>>&, const std::map<QString, QVariant>&)),
            this, SLOT(search_finished(const std::vector<std::shared_ptr<Show>>&, const std::map<QString, QVariant>&)));
    connect(_api.get(), SIGNAL(receive_thumbnail(const QImage&, const std::map<QString, QVariant>&)),
            this, SLOT(thumbnail_downloaded(const QImage&, const std::map<QString,QVariant>&)));
}

Search::~Search(){
    delete ui;
}

void Search::search(){
    _api->search_show(ui->_searchbar->text());
}
void Search::search_finished(const std::vector<std::shared_ptr<Show>>& shows, const std::map<QString, QVariant>& properties){
    _searchResults.reset(new SearchResults(shows));
    update_search_results(_searchResults->results());
}

void Search::update_search_results(const std::vector<std::shared_ptr<Show>>& shows){
    /* Clear scrollarea */
    QLayoutItem *item;
    while((item = ui->_grid->layout()->takeAt(0))){
        delete item->widget(); /* TODO: a bit weird review this */
        /* delete item; -> doesn't delete the widget it holds, strange !? */
    }
    /**/


    auto it = shows.begin();
    unsigned int i = 0;
    QGridLayout *gl = static_cast<QGridLayout*>(ui->_grid->layout());
    for(it = shows.begin(); it != shows.end(); it++){
        ThumbnailView *imgcontainer = new ThumbnailView(ui->_images->widget());
        qDebug() << (*it)->title();
        imgcontainer->set_showid((*it)->id());
        imgcontainer->set_title((*it)->title());
        connect(imgcontainer, SIGNAL(thumbnail_clicked(long)), this, SLOT(thumbnail_clicked(long)));
        gl->addWidget(imgcontainer, i / 4, i % 4);

        const QImage& thumbnail = get_thumbnail((*it)->id());
        if(!thumbnail.isNull()){
            imgcontainer->set_image(thumbnail);
        }
        i++;
    }

    const int count = ui->_grid->layout()->count();
    qDebug() << "Total:" << count;
    qDebug() << "Rows:" << count / 4 + 1;
    ui->_images->setFixedWidth(4 * ThumbnailView().maximumWidth());
}

void Search::update_thumbnail(long showid, const QImage& thumbnail){
    const unsigned int nbResults = (_searchResults == nullptr ? 0 : _searchResults->count());
    for(unsigned int i = 0; i < nbResults; i++){
        QLayoutItem *item = ui->_grid->layout()->itemAt(i);
        if(!item) continue;
        ThumbnailView *imgcontainer = static_cast<ThumbnailView*>(item->widget());
        if(imgcontainer && imgcontainer->showid() == showid){
            imgcontainer->set_image(thumbnail);
            break;
        }
    }
}

/*
QVariant Search::selected_show(){
    if(!ui->_searchOutput->currentItem()){
        return QVariant();
    }
    return ui->_searchOutput->currentItem()->data(Qt::UserRole);
}
*/

void Search::thumbnail_clicked(long showid){
    const Show& show = _searchResults->show(showid);
    emit request_show_information(show);
}

//
QImage Search::get_thumbnail(long showid){
    QString imgurl = _searchResults->show(showid).imgurl();
    QString filename = imgurl.split("/").back();
    if(imgurl.isEmpty() || filename.isEmpty()){
        return QImage();
    }

    if(ThumbnailCache::has(filename)){
        return ThumbnailCache::load(filename);
    }
    /* TODO: thumbnail caching system */
    /* TODO: search in cached thumbnails before downloading it */
    _api->thumbnail(showid, imgurl);
    return QImage(); /* Thumbnail is never in cache since there isn't one :p */
}
void Search::thumbnail_downloaded(const QImage& thumbnail, const std::map<QString, QVariant>& properties){
    long showid = properties.at("showid").value<long>();
    QString imgurl = properties.at("imgurl").value<QString>();
    ThumbnailCache::cache(imgurl.split("/").back(), thumbnail);
    update_thumbnail(showid, thumbnail);
}
