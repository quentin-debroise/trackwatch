#include <QStringList>
#include <QVariant>
#include <QDebug>
#include <QLabel>
#include <QImage>
#include <QResizeEvent>

#include <models/show.h>
#include <presenters/subscription.h>
#include <presenters/episodetracker.h>
#include <presenters/showinformation.h>
#include <ui_mainwindow.h>
#include <presenters/mainwindow.h>

MainWindow::MainWindow(QWidget *parent):
    QMainWindow(parent), ui(new Ui::MainWindow){
    ui->setupUi(this);
    _showInfoView = new ShowInformation(ui->_stacked);
    ui->_aside->addWidget(_showInfoView, 0, 0);

    connect(ui->_episodeTrackerView, SIGNAL(back()), this, SLOT(switch_page()));
    connect(ui->_searchView, SIGNAL(request_show_information(Show)),
            _showInfoView, SLOT(show_information(Show)));

    connect(_showInfoView, SIGNAL(subscribe(long)),
            ui->_subscriptions, SLOT(subscribe_to(long)));
    connect(_showInfoView, SIGNAL(unsubscribe(long)),
            ui->_subscriptions, SLOT(unsubscribe_to(long)));
    connect(ui->_subscriptions, SIGNAL(subscription_result(bool)),
            _showInfoView, SLOT(subscription_finished(bool)));
    connect(ui->_subscriptions, SIGNAL(unsubscription_result(bool)),
            _showInfoView, SLOT(subscription_finished(bool)));

    connect(ui->_subscriptions, SIGNAL(request_episode_tracker(long)),
            ui->_episodeTrackerView, SLOT(track(long)));
    connect(ui->_episodeTrackerView, SIGNAL(show_tracker()),
            this, SLOT(switch_page()));
}

MainWindow::~MainWindow(){
    delete ui;
    delete _showInfoView;
}

void MainWindow::switch_page(){
    ui->_stacked->setCurrentIndex((ui->_stacked->currentIndex() + 1) % ui->_stacked->count());
}
