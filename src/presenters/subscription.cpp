#include <QDebug>
#include <QMenu>
#include <QAction>
#include <QListWidget>
#include <QListWidgetItem>
#include <QSqlQuery>
#include <QSqlDatabase>
#include <QProgressBar>

#include <tvmazewrapper.h>
#include <models/subscriptionmodel.h>
#include <models/showepisode.h>
#include <presenters/subscription.h>
#include <ui_subscriptionview.h>


Subscription::Subscription(QWidget *parent):
    QWidget(parent),
    ui(new Ui::SubscriptionView),
    _api(std::unique_ptr<APIWrapper>(new TVMazeWrapper)){
    ui->setupUi(this);
    connect(_api.get(), SIGNAL(receive_single_show(Show, std::map<QString, QVariant>)),
            this, SLOT(subscribe(Show, std::map<QString, QVariant>)));
    connect(_api.get(), SIGNAL(receive_episodes(QList<Episode>, std::map<QString, QVariant>)),
            this, SLOT(register_episodes(QList<Episode>,std::map<QString,QVariant>)));
    connect(ui->_list, SIGNAL(clicked(QModelIndex)),
            this, SLOT(subscription_clicked(QModelIndex)));
    connect(ui->_list, SIGNAL(customContextMenuRequested(QPoint)),
            this, SLOT(handle_context_menu(QPoint)));

    load();
}

Subscription::~Subscription(){
    delete ui;
}

void Subscription::subscribe_to(long showid){
    /* TODO check when fail and rollback transaction */
    _api->get_show(showid);
    _api->get_episodes(showid);
}
void Subscription::subscribe(Show show, std::map<QString, QVariant> properties){
    show.save();
    SubscriptionModel newsub(show.id());
    if(newsub.save()){
        emit subscription_result(true);
        QListWidgetItem *newitem = new QListWidgetItem(show.title());
        newitem->setData(Qt::UserRole, QVariant::fromValue(show.id()));
        ui->_list->addItem(newitem);
    }
    else{
        emit subscription_result(false);
    }
}

void Subscription::register_episodes(QList<Episode> episodes, std::map<QString, QVariant> properties){
    QSqlDatabase::database().transaction();
    for (int i = 0; i < episodes.count(); i++){
        episodes.at(i).save();
        ShowEpisode se(episodes.at(i).id(), properties.at("showid").value<qlonglong>());
        se.save();
    }
    QSqlDatabase::database().commit();
}


void Subscription::unsubscribe_to(long showid){
    /* TODO: make transaction  if(delete1 && delete2) commit else rollback */
    Show::load(showid).remove();
    if(SubscriptionModel::load(showid).remove()){
        emit unsubscription_result(false);
        for(int i = 0; i < ui->_list->count(); i++){
            QListWidgetItem *item = ui->_list->item(i);
            qlonglong storedShowid = item->data(Qt::UserRole).value<qlonglong>();
            if(showid == storedShowid){
                ui->_list->model()->removeRow(i);
                break;
            }
        }
    }
    else{
        emit unsubscription_result(true);
    }
}

void Subscription::load() const{
    QList<SubscriptionModel> subs = SubscriptionModel::loadall();
    for (int i = 0; i < subs.count(); i++){
        Show s = Show::load(subs.at(i).showid());
        QListWidgetItem *newitem = new QListWidgetItem(s.title());
        newitem->setData(Qt::UserRole, QVariant::fromValue(s.id()));
        ui->_list->addItem(newitem);

        QProgressBar *progress = new QProgressBar;
        progress->setSizePolicy(QSizePolicy::Maximum, QSizePolicy::Fixed);
        progress->setFormat(s.title());
        progress->setMinimum(0);
        progress->setValue(25);
        ui->_list->setItemWidget(newitem, progress);
    }
}

void Subscription::subscription_clicked(QModelIndex modelIndex){
    qlonglong showid = modelIndex.data(Qt::UserRole).value<qlonglong>();
    emit request_episode_tracker(showid);
}

void Subscription::handle_context_menu(QPoint mousepos){
    QPoint globalpos = ui->_list->mapToGlobal(mousepos);
    QListWidgetItem *item = ui->_list->itemAt(mousepos);

    if(item){
        qlonglong showid = item->data(Qt::UserRole).value<qlonglong>();
        QMenu contextmenu;
        QAction *actionUnsubscribe = contextmenu.addAction(tr("Unsubscribe"));

        QAction *selected = contextmenu.exec(globalpos);
        if(selected == actionUnsubscribe){
            unsubscribe_to(showid);
        }
    }
}
