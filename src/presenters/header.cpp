#include <ui_headerview.h>
#include <presenters/header.h>

Header::Header(QWidget *parent):
    QWidget(parent),
    ui(new Ui::HeaderView){
    ui->setupUi(this);
}

Header::~Header(){
    delete ui;
}
