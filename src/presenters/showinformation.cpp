#include <QDebug>
#include <QImage>
#include <QPainter>

#include <models/show.h>
#include <thumbnailcache.h>
#include <models/subscriptionmodel.h>
#include <presenters/showinformation.h>
#include <ui_showinformationview.h>

ShowInformation::ShowInformation(QWidget *parent):
    QWidget(parent),
    ui(new Ui::ShowInformationView){
    ui->setupUi(this);
    connect(ui->_closeButton, SIGNAL(clicked()), this, SLOT(hide()));
    connect(ui->_subscribe, SIGNAL(clicked()), this, SLOT(subscribe_clicked()));
    setVisible(false);
}

ShowInformation::~ShowInformation(){
    delete ui;
}

void ShowInformation::show(){
    if(!isVisible()){
        QWidget::show();
    }
}

void ShowInformation::hide(){
    if(isVisible()){
        QWidget::hide();
    }
}

void ShowInformation::description_of(const Show& show){
    ui->_title->setText(show.title());
    ui->_description->setText(show.summary());
}

void ShowInformation::set_thumbnail(const QImage& thumbnail){
    ui->_thumbnail->setPixmap(QPixmap::fromImage(thumbnail));
}

void ShowInformation::subscribe_clicked(){
    emit subscribe(_showid);
}
void ShowInformation::unsubscribe_clicked(){
    emit unsubscribe(_showid);
}

void ShowInformation::toggle_subscription(bool active) const{
    disconnect(ui->_subscribe, SIGNAL(clicked()), 0, 0);
    if(active){
        ui->_subscribe->set_checked(false);
        // ui->_subscribe->setText(tr("Subscribe"));
        connect(ui->_subscribe, SIGNAL(clicked()), this, SLOT(subscribe_clicked()));
    }
    else{
        ui->_subscribe->set_checked(true);
        // ui->_subscribe->setText(tr("Unsubscribe"));
        connect(ui->_subscribe, SIGNAL(clicked()), this, SLOT(unsubscribe_clicked()));
    }
}

void ShowInformation::show_information(const Show &s){
    if(SubscriptionModel::exists(s.id())){
        toggle_subscription(false);
    }
    else{
        toggle_subscription(true);
    }

    _showid = s.id();
    description_of(s);
    if(ThumbnailCache::has(s.imgpath())){
        QImage image = ThumbnailCache::load(s.imgpath());
        set_thumbnail(image);
    }
    show();
}

void ShowInformation::subscription_finished(bool success){
    toggle_subscription(!success);
}

void ShowInformation::paintEvent(QPaintEvent *event){
    QStyleOption opt;
    opt.init(this);
    QPainter p(this);
    style()->drawPrimitive(QStyle::PE_Widget, &opt, &p, this);
}
