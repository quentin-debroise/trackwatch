#include <QApplication>
#include <QNetworkProxyFactory>
#include <QNetworkProxy>
#include <QSqlDatabase>
#include <QSqlQuery>
#include <QFile>

#include <thumbnailcache.h>
#include <presenters/mainwindow.h>

int main(int argc, char *argv[]){
    QApplication app(argc, argv);

    /*
    QNetworkProxyFactory::setUseSystemConfiguration(true);
    QNetworkProxy proxy;
    proxy.setType(QNetworkProxy::Socks5Proxy);
    proxy.setType(QNetworkProxy::HttpProxy);
    proxy.setHostName("proxy.ecole.ensicaen.fr");
    proxy.setPort(3128);
    proxy.setUser("debroise");
    proxy.setPassword("q79$3:Ag31)8");
    QNetworkProxy::setApplicationProxy(proxy);
    */

    /* Init thumbnail cache */
    ThumbnailCache::init();

    /* Set database connection */
    QSqlDatabase _conn = QSqlDatabase::addDatabase("QSQLITE");
    _conn.setDatabaseName(QCoreApplication::applicationDirPath().append("/db.sqlite"));
    if(!_conn.open()){
        qDebug() << "main.cpp [FATAL]: Can't open database";
    }
    _conn.exec("PRAGMA foreign_keys = ON").exec();

    /* Set stylesheet */
    QFile stylesheet(":/style/style.qss");
    if(stylesheet.open(QFile::ReadOnly)){
        app.setStyleSheet(stylesheet.readAll());
    }
    else{
        qDebug() << "main.cpp [ERROR]: Can't open stylesheet";
    }

    /* Run */
    MainWindow mw;
    mw.show();
    return app.exec();
}

