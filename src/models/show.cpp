#include <QDebug>
#include <QSqlError>
#include <QStringList>
#include <QSqlQuery>
#include <query/querybuilder.h>
#include <models/show.h>
#include <query/insertstatement.h>
#include <query/deletestatement.h>

Show Show::load(qlonglong showid){
    SelectStatement sq = QueryBuilder::create_select();
    QSqlQuery query = sq.select("Show").where("id", showid).get();
    query.setForwardOnly(true);
    query.exec();
    query.next();
    return from_record(query.record());
}

Show Show::from_record(QSqlRecord record){
    if(record.isEmpty()){
        return Show();
    }

    return Show(
            record.value("id").toLongLong(),
            record.value("title").toString(),
            record.value("status").toString(),
            record.value("summary").toString(),
            record.value("imgurl").toString());
}

bool Show::save() const{
    InsertStatement ins;
    QSqlQuery query = ins.insert("Show")
            .columns()
            .values({
                _id,
                _title,
                "Running",
                _summary,
                _imgurl,
            })
            .get();
    return query.exec();
}

bool Show::remove() const{
    DeleteStatement del;
    QSqlQuery query = del.remove("Show").where("id", _id).get();
    return query.exec();
}

QList<Episode> Show::episodes() const{
    QList<Episode> episodes;
    SelectStatement sq = QueryBuilder::create_select();
    QSqlQuery query = sq.select("ShowEpisode", {"episode_id"}).where("show_id", _id).get();
    query.exec();
    while(query.next()){
        episodes.append(Episode::load(query.value("episode_id").toLongLong()));
    }
    return episodes;
}


Show::Show(qlonglong id, QString title):
    _id(id), _title(title){
}

Show::Show(qlonglong id, QString title, QString status, QString summary, QString imgurl):
    _id(id), _title(title), _status(status), _summary(summary){
    set_image(imgurl);
}

/*
Show::Show(const Show& other):
    _id(other._id), _title(other._title), _summary(other._summary){
    set_image(other._imgurl);
}
*/

void Show::set_image(const QString& imgurl){
    _imgurl = imgurl;
    _imgpath = _imgurl.split("/").back();
}
