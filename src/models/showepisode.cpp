#include <QSqlQuery>
#include <query/querybuilder.h>
#include <query/insertstatement.h>
#include <query/updatestatement.h>
#include <models/showepisode.h>

ShowEpisode ShowEpisode::load(qlonglong episodeId){
    SelectStatement sq = QueryBuilder::create_select();
    QSqlQuery query = sq.select("ShowEpisode")
                        .where("episode_id", episodeId)
                        .get();
    query.exec();
    query.next();
    return ShowEpisode::from_record(query.record());
}

ShowEpisode ShowEpisode::from_record(QSqlRecord record){
    if(record.isEmpty()){
        return ShowEpisode();
    }
    return ShowEpisode(
                record.value("episode_id").toLongLong(),
                record.value("show_id").toLongLong(),
                record.value("watched").toBool());
}

bool ShowEpisode::save() const{
    InsertStatement ins;
    QSqlQuery query = ins.insert("ShowEpisode")
                         .columns({"episode_id", "show_id"})
                         .values({_episodeId, _showId})
                         .get();
    return query.exec();
}

bool ShowEpisode::update(QMap<QString, QVariant> newvalues) const{
    QMap<QString, QVariant> allcolumns;
//    allcolumns["episode_id"] = newvalues.keys().contains("episode_id") ?
//                               newvalues["episode_id"] : _episodeId;
//    allcolumns["show_id"] = newvalues.keys().contains("show_id") ?
//                               newvalues["show_id"] : _showId;
    allcolumns["watched"] = newvalues.keys().contains("watched") ?
                               newvalues["watched"] : _watched;
    UpdateStatement upd;
    upd.update("ShowEpisode");
    QMap<QString, QVariant>::const_iterator it;
    for (it = newvalues.constBegin(); it != newvalues.constEnd(); it++){
        upd.set(it.key(), it.value());
    }
    QSqlQuery query = upd.where("episode_id", _episodeId).get();
    return query.exec();
}

Episode ShowEpisode::episode() const{
    return Episode::load(_episodeId);
}
Show ShowEpisode::show() const{
    return Show::load(_showId);
}

ShowEpisode::ShowEpisode(qlonglong episodeId, qlonglong showId, bool watched):
    _episodeId(episodeId), _showId(showId), _watched(watched){
}
