#include <QDebug>
#include <QSqlQuery>

#include <query/selectstatement.h>
#include <query/insertstatement.h>
#include <query/deletestatement.h>
#include <models/subscriptionmodel.h>

bool SubscriptionModel::exists(qlonglong showid){
    SubscriptionModel sub = SubscriptionModel::load(showid);
    return !sub.is_null();
}

SubscriptionModel SubscriptionModel::load(qlonglong showid){
    SelectStatement sel;
    QSqlQuery query = sel.select("Subscription")
                         .where("showid", showid)
                         .get();
    query.exec();
    return query.first() ? from_record(query.record()) : SubscriptionModel();
}

QList<SubscriptionModel> SubscriptionModel::loadall(){
    SelectStatement sel;
    QSqlQuery query = sel.select("Subscription").get();
    query.exec();
    QList<SubscriptionModel> subscriptions;
    while (query.next()){
        subscriptions.append(from_record(query.record()));
    }
    return subscriptions;
}

SubscriptionModel SubscriptionModel::from_record(QSqlRecord record){
    if(record.isEmpty()){
        return SubscriptionModel();
    }
    return SubscriptionModel(
                record.value("id").toLongLong(),
                record.value("showid").toLongLong());
}

SubscriptionModel::SubscriptionModel(qlonglong showid):
    _showid(showid){
}

SubscriptionModel::SubscriptionModel(qlonglong id, qlonglong showid):
        _id(id), _showid(showid){
}

Show SubscriptionModel::show() const{
    return Show::load(_showid);
}

bool SubscriptionModel::save() const{
    InsertStatement ins;
    QSqlQuery query = ins.insert("Subscription")
                         .columns({"showid"})
                         .values({_showid})
                         .get();
    return query.exec();
}

bool SubscriptionModel::remove() const{
    DeleteStatement del;
    QSqlQuery query = del.remove("Subscription").where("showid", _showid).get();
    return query.exec();
}


