#include <QDebug>
#include <QSqlError>
#include <query/querybuilder.h>
#include <query/insertstatement.h>
#include <models/showepisode.h>
#include <models/episode.h>

Episode Episode::load(qlonglong id){
    SelectStatement sq = QueryBuilder::create_select();
    QSqlQuery query = sq.select("Episode").where("id", id).get();
    query.exec();
    query.next();
    return from_record(query.record());
}

Episode Episode::from_record(QSqlRecord record){
    if(record.isEmpty()){
        return Episode();
    }
    return Episode(
            record.value("id").toLongLong(),
            record.value("title").toString(),
            record.value("season").toInt(),
            record.value("epnumber").toInt(),
            record.value("runtime").toInt());
}

Episode::Episode(qlonglong id, const QString& title, int season, int numberInSeason, int runtime):
    _id(id), _title(title), _season(season), _numberInSeason(numberInSeason), _runtime(runtime){
}

bool Episode::save() const{
    InsertStatement ins;
    QSqlQuery query = ins.insert("Episode")
                         .columns({"id", "title", "season", "epnumber", "runtime"})
                         .values({_id, _title, _season, _numberInSeason, _runtime})
                         .get();
    /* TODO: also insert into ShowEpisode */
    if(!query.exec()){
        qDebug() << query.lastError() << query.lastQuery() << _id;
    }
    return true;
}
