#include <models/show.h>
#include <models/searchresults.h>

SearchResults::SearchResults(const std::vector<std::shared_ptr<Show>>& shows):
    _shows(shows){
    auto it = _shows.begin();
    while(it != _shows.end()){
        _showsmap[(*it)->id()] = (*it).get();
        it++;
    }
}
