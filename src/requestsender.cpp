#include <QDebug>
#include <QNetworkAccessManager>
#include <QUrlQuery>
#include <QNetworkRequest>

#include <requestsender.h>

QNetworkAccessManager *RequestSender::_nam = nullptr;

RequestSender::RequestSender(){
    _request.setSslConfiguration(QSslConfiguration::defaultConfiguration());
    if(!_nam){
        _nam = new QNetworkAccessManager;
    }
}

RequestSender::~RequestSender(){
}

void RequestSender::add_property(const QString& key, const QVariant& value){
    _properties[key] = value;
}

void RequestSender::send(){
    _request.setOriginatingObject(this);
    _reply = _nam->get(_request);
    connect(_reply, SIGNAL(finished()), this, SLOT(finished()));
    connect(_reply, SIGNAL(error(QNetworkReply::NetworkError)), this, SLOT(error(QNetworkReply::NetworkError)));
}

void RequestSender::request(QUrl url, const std::map<QString, QString>& params){
   if(!params.empty()){
       QUrlQuery query;
       auto it = params.begin();
       while(it != params.end()){
           query.addQueryItem(it->first, it->second);
           it++;
       }
       url.setQuery(query);
   }
   _request.setUrl(url);
}


void RequestSender::finished(){
    _reply->deleteLater();

    if(_reply->error()){
        qDebug() << "requestsender.cpp [ERROR]: " + _reply->errorString();
        return;
    }

    int status = _reply->attribute(QNetworkRequest::HttpStatusCodeAttribute).toInt();
    if(status != 200){
        qDebug() << "requestsender.cpp [ERROR]: Request to " + _reply->url().toString() + " failed. Status code isn't 200";
        return;
    }
    else if(status >= 300 && status < 400){
        qDebug() << "requestsender.cpp [WARNING]: Request redirected";
        QUrl redirectURL = _reply->attribute(QNetworkRequest::RedirectionTargetAttribute).toUrl();
        qDebug() << "\t" + _reply->url().toString() +  " --redirected-to--> " + _reply->url().resolved(redirectURL).toString();
        return;
    }

    if(_request.originatingObject() == this){
        qDebug() << "requestsender.cpp [INFO]: GET Request success to " + _reply->url().toString();
        QByteArray data = _reply->readAll();
        emit request_success(data, _properties);
    }
}

void RequestSender::error(QNetworkReply::NetworkError err){
    emit request_failure();
}
