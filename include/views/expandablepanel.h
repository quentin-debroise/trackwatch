#ifndef EXPANDABLEPANEL_H
#define EXPANDABLEPANEL_H

#include <QWidget>
#include <QPushButton>
#include <QListWidget>
#include <QTableWidget>
#include <QBoxLayout>

class ExpandablePanel: public QWidget{
    Q_OBJECT
public:
    ExpandablePanel(QWidget *parent=0);
    ~ExpandablePanel();
    void add(int episodeId, int numberInSeason, QString title, bool checked=false);
    void set_category(QString name);

public slots:
    void toggle();
private slots:
    void finished_expanding();
    void finished_collapsing();
    void clicked(bool active);

private:
    QPushButton *_category;
    QTableWidget *_hidden;
    QBoxLayout *_layout;
    bool animating;

    void setup_ui();

signals:
    void watched(qlonglong episodeId, bool watched);
};

#endif // EXPANDABLEPANEL_H
