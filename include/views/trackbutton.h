#ifndef TRACKBUTTON_H
#define TRACKBUTTON_H

#include <QWidget>

class QLabel;
class QBoxLayout;
class QMouseEvent;
class QEvent;

class TrackButton: public QWidget{
    Q_OBJECT
public:
    TrackButton(QWidget *parent=0);
    ~TrackButton();

    virtual void mousePressEvent(QMouseEvent *event) override;
    virtual bool eventFilter(QObject *target, QEvent *event) override;
    virtual void paintEvent(QPaintEvent *event) override;

    void set_checked(bool checked);

private:
    QBoxLayout *_layout;
    QLabel *_before;
    QLabel *_text;
    bool _checked;
    bool _hovering = false;

    void setup_ui();
    void handle_check(bool checked);

signals:
    void clicked();
};

#endif // TRACKBUTTON_H
