#ifndef EPISODEVIEW_H
#define EPISODEVIEW_H

#include <QWidget>

namespace Ui{
    class EpisodeView;
}

class EpisodeView: public QWidget{
    Q_OBJECT
public:
    EpisodeView(QString title, int numberInSeason, QWidget *parent=0);

private slots:
    void episode_watch_update(bool checked);

private:
    Ui::EpisodeView *ui;
};

#endif // EPISODEVIEW_H
