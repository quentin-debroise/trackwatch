#ifndef THUMBNAIL_H
#define THUMBNAIL_H

#include <QWidget>

namespace Ui{
    class ThumbnailView;
}

class ThumbnailView: public QWidget{
    Q_OBJECT
public:
    ThumbnailView(QWidget *parent=0);
    ~ThumbnailView() = default;

    long showid() const{return _showid;}

    void set_showid(long showid){_showid = showid;}
    void set_title(const QString& title);
    void set_image(const QImage& image);

    void mousePressEvent(QMouseEvent *event) override;

signals:
    void thumbnail_clicked(long showid);

private:
    Ui::ThumbnailView *ui;
    long _showid;
};

#endif // THUMBNAIL_H
