#ifndef TVMAZEWRAPPER_H
#define TVMAZEWRAPPER_H

#include <vector>
#include <QByteArray>
#include <QJsonArray>

#include <apiwrapper.h>


class TVMazeWrapper: public APIWrapper{
    Q_OBJECT
public:
    TVMazeWrapper();
    virtual ~TVMazeWrapper();

    virtual void search_show(const QString& show) override;
    virtual void thumbnail(long showid, const QString& imgurl) override;
    virtual void get_show(long showid) override;
    virtual void get_episodes(long showid) override;

private slots:
    virtual void parse_search(QByteArray data, std::map<QString, QVariant> properties) override;
    virtual void parse_thumbnail(QByteArray data, std::map<QString, QVariant> properties) override;
    virtual void parse_single_show(QByteArray data, std::map<QString, QVariant> properties) override;
    virtual void parse_episodes(QByteArray data, std::map<QString, QVariant> properties) override;
private:
    static QString API_URL;

};

#endif // TVMAZEWRAPPER_H
