#ifndef THUMBNAILCACHE_H
#define THUMBNAILCACHE_H

class QImage;
class QString;

class ThumbnailCache{
public:
    static void init();
    static void cache(const QString& filename, const QImage& imgdata);
    static QImage load(const QString& filename);
    static bool has(const QString& filename);
private:
    ThumbnailCache() = default;
};

#endif // THUMBNAILCACHE_H
