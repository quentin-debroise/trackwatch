#ifndef INSERTSTATEMENT_H
#define INSERTSTATEMENT_H

#include <QList>
#include <QStringList>
#include <QVariant>
#include <QSqlQuery>

class InsertStatement{
public:
    InsertStatement() = default;
    InsertStatement& insert(QString tablename);
    InsertStatement& columns(QStringList columns=QStringList());
    InsertStatement& values(QList<QVariant> values);
    QSqlQuery get() const;
    void clear();

    QString toString() const;

private:
    QString _tablename;
    QStringList _columns;
    QList<QVariant> _values;
};

#endif // INSERTSTATEMENT_H
