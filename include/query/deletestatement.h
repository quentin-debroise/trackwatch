#ifndef DELETESTATEMENT_H
#define DELETESTATEMENT_H

#include <QSqlQuery>
#include <query/whereconditiondao.h>

class DeleteStatement{
public:
    DeleteStatement() = default;
    DeleteStatement& remove(QString tablename);
    DeleteStatement& where(QString attribute, QVariant value);
    QSqlQuery get() const;

    void clear();
    QString toString() const;

private:
    QString _tablename;
    QList<WhereConditionDAO> _where;
};

#endif // DELETESTATEMENT_H
