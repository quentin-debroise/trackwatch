#ifndef UPDATESTATEMENT_H
#define UPDATESTATEMENT_H

#include <QSqlQuery>
#include <query/whereconditiondao.h>

class UpdateStatement{
public:
    UpdateStatement() = default;
    UpdateStatement& update(QString tablename);
    UpdateStatement& set(QString column, QVariant newvalue);
    UpdateStatement& where(QString attribute, QVariant value);
    QSqlQuery get() const;
    void clear();
    QString toString() const;

private:
    QString _tablename;
    QMap<QString, QVariant> _set;
    QList<WhereConditionDAO> _where;
};

#endif // UPDATESTATEMENT_H
