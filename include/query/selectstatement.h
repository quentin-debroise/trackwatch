#ifndef SELECTSTATEMENT_H
#define SELECTSTATEMENT_H

#include <QSqlQuery>
#include <query/whereconditiondao.h>

class SelectStatement{
public:
    SelectStatement() = default;
    SelectStatement select(QString tablename, const QStringList& attributes=QStringList());
    SelectStatement where(QString attribute, QVariant value);
    SelectStatement w_and(QString attribute, QVariant value);
    SelectStatement w_or(QString attribute, QVariant value);
    QSqlQuery get() const;
    void clear();

    QString toString() const;

private:
    QString _tablename;
    QStringList _attributes;
    QList<WhereConditionDAO> _where;
};

#endif // SELECTSTATEMENT_H
