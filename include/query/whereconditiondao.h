#ifndef WHERECONDITIONDAO_H
#define WHERECONDITIONDAO_H

#include <QString>
#include <QVariant>

class WhereConditionDAO{
public:
    enum WhereOp{
        OP_EQ = 0,
        OP_NE = 1,
        OP_LT = 2,
        OP_GT = 3,
        OP_LE = 4,
        OP_GE = 5
    };

    enum WhereLogic{
        LOGIC_NONE = 0,
        LOGIC_AND = 1,
        LOGIC_OR = 2,
        LOGIC_IN = 3,
        LOGIC_LIKE = 4
    };

    WhereConditionDAO(
            QString columnname,
            QVariant bindvalue,
            WhereOp op=OP_EQ,
            WhereLogic logic=LOGIC_AND,
            bool subquery=false);

    QString placeholder() const{return ":" + _columnname;}
    const QVariant& value() const{return _bindvalue;}
    QString toString() const;

private:
    QString _columnname;
    QVariant _bindvalue;
    WhereOp _op;
    WhereLogic _logic;
    bool _subquery;
};

#endif // WHERECONDITIONDAO_H
