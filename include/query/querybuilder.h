#ifndef QUERYBUILDER_H
#define QUERYBUILDER_H

#include <memory>
#include <query/selectstatement.h>

class QueryBuilder{
public:
    static SelectStatement create_select();
};

#endif // QUERYBUILDER_H
