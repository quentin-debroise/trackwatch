#ifndef HEADER_H
#define HEADER_H

#include <QWidget>

namespace Ui{
    class HeaderView;
}

class Header: public QWidget{
public:
    Header(QWidget *parent=0);
    ~Header();

private:
    Ui::HeaderView *ui;
};

#endif // HEADER_H
