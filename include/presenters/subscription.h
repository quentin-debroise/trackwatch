#ifndef SUBSCRIPTION_H
#define SUBSCRIPTION_H

#include <memory>
#include <QWidget>
#include <apiwrapper.h>
#include <QModelIndex>

namespace Ui{
    class SubscriptionView;
}

class QListWidget;

class Subscription: public QWidget{
    Q_OBJECT
public:
    Subscription(QWidget* parent=0);
    ~Subscription();
    void load() const;

public slots:
    void subscribe_to(long showid);
    void unsubscribe_to(long showid);
    void handle_context_menu(QPoint mousepos);
    // void update_progress(qlonglong showid);
private slots:
    void subscribe(Show show, std::map<QString, QVariant> properties);
    void register_episodes(QList<Episode> episodes, std::map<QString, QVariant> properties);
    void subscription_clicked(QModelIndex modelIndex);

private:
    Ui::SubscriptionView *ui;
    std::unique_ptr<APIWrapper> _api;

signals:
    void subscription_result(bool success);
    void unsubscription_result(bool failed);
    void request_episode_tracker(long showid);

};

#endif // SUBSCRIPTION_H
