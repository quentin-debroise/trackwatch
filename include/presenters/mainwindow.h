#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <vector>
#include <QMainWindow>
#include <QModelIndex>

#include <models/show.h>


namespace Ui{
    class MainWindow;
}

class ShowInformation;

class MainWindow: public QMainWindow{
    Q_OBJECT
public:
    explicit MainWindow(QWidget *parent=0);
    ~MainWindow();

public slots:
    void switch_page();

private:
    Ui::MainWindow *ui;
    ShowInformation *_showInfoView;
};

#endif // MAINWINDOW_H
