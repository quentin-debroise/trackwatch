#ifndef SHOWDESCRIPTION_H
#define SHOWDESCRIPTION_H

#include <QWidget>

namespace Ui{
    class ShowInformationView;
}

class QPaintEvent;
class QImage;
class Show;

class ShowInformation: public QWidget{
    Q_OBJECT
public:
    explicit ShowInformation(QWidget *parent=0);
    ~ShowInformation();
    virtual void paintEvent(QPaintEvent *event) override;

    void description_of(const Show& show);

public slots:
    void show();
    void hide();
    void set_thumbnail(const QImage& thumbnail);
    void toggle_subscription(bool active) const;
    void show_information(const Show& s);
    void subscription_finished(bool success);
private slots:
    void subscribe_clicked();
    void unsubscribe_clicked();

private:
    Ui::ShowInformationView *ui;
    long _showid;

signals:
    void subscribe(long showid);
    void unsubscribe(long showid);
};

#endif // SHOWDESCRIPTION_H
