#ifndef SEARCHVIEW_H
#define SEARCHVIEW_H

#include <memory>
#include <QWidget>
#include <QModelIndex>
#include <QByteArray>
#include <models/show.h>
#include <apiwrapper.h>

namespace Ui{
    class SearchView;
}

class SearchResults;

class Search : public QWidget{
    Q_OBJECT
public:
    explicit Search(QWidget *parent=0);
    ~Search();
    void update_search_results(const std::vector<std::shared_ptr<Show>>& shows);
    QVariant selected_show();
    void update_thumbnail(long showid, const QImage& image);
    QImage get_thumbnail(long showid);

private slots:
    void search();
    void search_finished(const std::vector<std::shared_ptr<Show>>& shows, const std::map<QString, QVariant>& properties);
    void thumbnail_clicked(long showid);
    void thumbnail_downloaded(const QImage& thumbnail, const std::map<QString, QVariant>& properties);

private:
    Ui::SearchView *ui;
    unsigned int resultsNbColumns;

    std::unique_ptr<SearchResults> _searchResults;
    std::unique_ptr<APIWrapper> _api;

signals:
    void request_show_information(const Show&);
};


#endif // SEARCHVIEW_H
