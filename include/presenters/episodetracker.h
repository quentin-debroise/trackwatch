#ifndef EPISODETRACKER_H
#define EPISODETRACKER_H

#include <QWidget>

namespace Ui{
    class EpisodeTrackerView;
}

class EpisodeTracker: public QWidget{
    Q_OBJECT
public:
    EpisodeTracker(QWidget *parent=0);
    ~EpisodeTracker();

public slots:
    void track(long showid);
private slots:
    void back_clicked();
    void watched(qlonglong episodeId, bool watched);

private:
    Ui::EpisodeTrackerView *ui;

    void clear();

signals:
    void back();
    void show_tracker();

};

#endif // EPISODETRACKER_H
