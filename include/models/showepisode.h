#ifndef SHOWEPISODE_H
#define SHOWEPISODE_H

#include <QMetaType>
#include <QSqlRecord>
#include <models/show.h>
#include <models/episode.h>

class ShowEpisode{
public:
    static ShowEpisode load(qlonglong episodeId);
    static ShowEpisode from_record(QSqlRecord record);
public:
    ShowEpisode() = default;
    ShowEpisode(qlonglong episodeId, qlonglong showId, bool watched=false);

    bool is_empty() const{return _episodeId == -1 && _showId == -1;}
    bool watched() const{return _watched;}
    Episode episode() const;
    Show show() const;

    bool save() const;
    bool update(QMap<QString, QVariant> newvalues) const;

private:
    qlonglong _episodeId = -1;
    qlonglong _showId = -1;
    bool _watched;
};

Q_DECLARE_METATYPE(ShowEpisode)

#endif // SHOWEPISODE_H
