#ifndef SHOW_H
#define SHOW_H

#include <vector>
#include <QMetaType>
#include <QString>
#include <QSqlRecord>
#include <models/episode.h>

class Show{
public:
    static Show load(qlonglong showid);
    static Show from_record(QSqlRecord record);
public:
    Show() = default;
    // Show(const Show& other);
    Show(qlonglong id, QString title);
    Show(qlonglong id, QString title, QString status, QString summary, QString imgurl);
    ~Show() = default;

    qlonglong id() const{return _id;}
    const QString& title() const{return _title;}
    const QString& imgurl() const{return _imgurl;}
    const QString& imgpath() const{return _imgpath;}
    const QString& summary() const{return _summary;}
    QList<Episode> episodes() const;

    bool is_empty() const{return _id == -1;}

    bool save() const;
    bool remove() const;

private:
    qlonglong _id = -1;
    QString _title;
    QString _imgurl;
    QString _status;
    QString _summary;
    QString _imgpath;

    void set_image(const QString& imgurl);
};

Q_DECLARE_METATYPE(Show)

#endif // SHOW_H
