#ifndef EPISODE_H
#define EPISODE_H

#include <QMetaType>
#include <QString>
#include <QSqlRecord>

class Episode{
public:
    static Episode load(qlonglong id);
    static Episode from_record(QSqlRecord record);
public:
    Episode() = default;
    Episode(qlonglong id, const QString& title, int season, int numberInSeason, int runtime);

    qlonglong id() const{return _id;}
    const QString& title() const{return _title;}
    int season() const{return _season;}
    int number_in_season() const{return _numberInSeason;}
    int runtime() const{return _runtime;}

    bool save() const;

private:
    qlonglong _id = -1;
    QString _title;
    int _season;
    int _numberInSeason;
    int _runtime;
};

Q_DECLARE_METATYPE(Episode)

#endif // EPISODE_H
