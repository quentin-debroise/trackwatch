#ifndef SUBSCRIPTIONMODEL_H
#define SUBSCRIPTIONMODEL_H

#include <QSqlRecord>
#include <models/show.h>

class SubscriptionModel{
public:
    static SubscriptionModel load(qlonglong showid);
    static QList<SubscriptionModel> loadall();
    static SubscriptionModel from_record(QSqlRecord record);
    static bool exists(qlonglong showid);
public:
    SubscriptionModel() = default;
    SubscriptionModel(qlonglong showid);
    SubscriptionModel(qlonglong id, qlonglong showid);
    bool is_null() const{return _id == -1;}
    Show show() const;
    bool save() const;
    bool remove() const;

    qlonglong showid() const{return _showid;}

private:;
    qlonglong _id = -1;
    qlonglong _showid;
};

#endif // SUBSCRIPTIONMODEL_H
