#ifndef SEARCHRESULTS_H
#define SEARCHRESULTS_H

#include <map>
#include <vector>
#include <memory>

class Show;


class SearchResults{
public:
    SearchResults(const std::vector<std::shared_ptr<Show>>& shows);
    const std::vector<std::shared_ptr<Show>>& results() const{return _shows;}
    const Show& show(long showid) const{return *(_showsmap.at(showid));}
    unsigned int count() const{return _shows.size();}

private:
    std::vector<std::shared_ptr<Show>> _shows;
    std::map<long, Show*> _showsmap;
};

#endif // SEARCHRESULTS_H
