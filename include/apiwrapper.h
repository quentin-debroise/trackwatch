#ifndef APIWRAPPER_H
#define APIWRAPPER_H

#include <vector>
#include <memory>
#include <QObject>
#include <QVariant>
#include <QImage>
#include <QJsonArray>
#include <QList>

#include <models/episode.h>
#include <models/show.h>


class APIWrapper: public QObject{
    Q_OBJECT
public:
    APIWrapper() = default;
    ~APIWrapper() = default;

    virtual void search_show(const QString& search) = 0;
    virtual void thumbnail(long showid, const QString& show) = 0;
    virtual void get_show(long showid) = 0;
    virtual void get_episodes(long showid) = 0;

public slots:
    virtual void parse_search(QByteArray data, std::map<QString, QVariant> properties) = 0;
    virtual void parse_thumbnail(QByteArray data, std::map<QString, QVariant> properties) = 0;
    virtual void parse_single_show(QByteArray data, std::map<QString, QVariant> properties) = 0;
    virtual void parse_episodes(QByteArray data, std::map<QString, QVariant> properties) = 0;

signals:
    void receive_shows(std::vector<std::shared_ptr<Show>>, std::map<QString, QVariant> properties=std::map<QString, QVariant>());
    void receive_thumbnail(QImage image, std::map<QString, QVariant> properties=std::map<QString, QVariant>());
    void receive_single_show(Show show, std::map<QString, QVariant> properties=std::map<QString, QVariant>());
    void receive_episodes(QList<Episode> episodes, std::map<QString, QVariant> properties=std::map<QString, QVariant>());
};

#endif // APIWRAPPER_H
