#ifndef REQUESTSENDER_H
#define REQUESTSENDER_H

#include <QObject>
#include <QByteArray>
#include <QNetworkRequest>
#include <QNetworkReply>
#include <QUrl>

class QNetworkAccessManager;


class RequestSender: public QObject{
    Q_OBJECT
public:
    RequestSender();
    ~RequestSender();
    void send();
    void request(QUrl url, const std::map<QString, QString>& params=std::map<QString, QString>());
    void add_property(const QString& key, const QVariant& value);

private slots:
    void finished();
    void error(QNetworkReply::NetworkError err);
private:
    static QNetworkAccessManager *_nam;
    QNetworkReply *_reply;
    QNetworkRequest _request;
    std::map<QString, QVariant> _properties;

signals:
    void request_success(QByteArray data, std::map<QString, QVariant> properties);
    void request_failure();
};

#endif // REQUESTSENDER_H
