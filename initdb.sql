DROP TABLE IF EXISTS Show;
DROP TABLE IF EXISTS Episode;
DROP TABLE IF EXISTS Subscription;
DROP TABLE IF EXISTS ShowEpisode;
DROP TABLE IF EXISTS UserInfo;
DROP VIEW IF EXISTS ShowStat;

CREATE TABLE UserInfo(
    totalRuntime BIGINT NOT NULL CHECK(totalRuntime >= 0) DEFAULT 0
);

CREATE TABLE Show(
    id INTEGER PRIMARY KEY,
    title VARCHAR(128) NOT NULL,
    status VARCHAR(20) CHECK(status IN ("Running", "Ended")) NOT NULL,
    summary TEXT,
    imgurl VARCHAR
);

CREATE TABLE ShowEpisode(
    episode_id INTEGER,
    show_id INTEGER,
    watched BOOLEAN NOT NULL DEFAULT 0,

    PRIMARY KEY(episode_id, show_id),
    FOREIGN KEY(episode_id) REFERENCES Episode(id),
    FOREIGN KEY(show_id) REFERENCES Show(id)
);

CREATE TABLE Episode(
    id INTEGER PRIMARY KEY,
    title VARCHAR(255) NOT NULL,
    season INTEGER NOT NULL CHECK(season >= 0),
    epnumber INTEGER NOT NULL CHECK(epnumber >= 0),
    runtime INTEGER NOT NULL CHECK(runtime >= 0),
    airstamp TIMESTAMP
);

CREATE TABLE Subscription(
    id INTEGER PRIMARY KEY,
    showid INTEGER UNIQUE NOT NULL,

    FOREIGN KEY(showid) REFERENCES Show(id)
);

CREATE VIEW IF NOT EXISTS ShowStat AS
    SELECT show_id,
           COUNT(*) AS total,
           SUM(CASE WHEN watched = 0 THEN 1 ELSE 0 END) AS totalNotWatched,
           SUM(CASE WHEN watched = 1 THEN 1 ELSE 0 END) AS totalWatched
    FROM ShowEpisode
    GROUP BY show_id;

CREATE TRIGGER IF NOT EXISTS Trigg_DeleteShow
BEFORE DELETE ON Show
FOR EACH ROW
BEGIN
    DELETE FROM Subscription WHERE showid = old.id;
    DELETE FROM Episode WHERE id IN (
        SELECT episode_id FROM ShowEpisode WHERE show_id = old.id
    );
    DELETE FROM ShowEpisode WHERE show_id = old.id;
END;

